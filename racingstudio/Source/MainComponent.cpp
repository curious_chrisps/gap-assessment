#include "MainComponent.h"
MainContentComponent::MainContentComponent()    :   system(nullptr), collisionWait (100)
{
    // ======================= JUCE INTERFACE INITIALISATION =======================
    for (int i = 0; i < NUM_SLIDERS; i++)
    {
        labelsArray[i].setText(labelStrings[i], dontSendNotification);
        labelsArray[i].attachToComponent(&groupSliders[i], true);
        
        groupSliders[i].setSliderStyle(juce::Slider::LinearBar);
        groupSliders[i].setRange(0.0, 1.0);
        groupSliders[i].addListener(this);
        addAndMakeVisible(groupSliders[i]);
    }
    
    masterGroupsLabel.setText("Master", dontSendNotification);
    masterGroupsLabel.setJustificationType(juce::Justification::centred);
    addAndMakeVisible(masterGroupsLabel);
    
    carSoundsLabel.setText("Car", dontSendNotification);
    carSoundsLabel.setJustificationType(juce::Justification::centred);
    addAndMakeVisible(carSoundsLabel);
    
    environmentSoundsLabel.setText("Environment", dontSendNotification);
    environmentSoundsLabel.setJustificationType(juce::Justification::centred);
    addAndMakeVisible(environmentSoundsLabel);
    
    setSize (500, 500);
    
    // launches the other, game, app
    launchGame();
    
    
    // Some slider set up before the start of the game
    for (int i = 0; i < NUM_SLIDERS; i++)
    {
        if (i == 2)
        {
            groupSliders[i].setValue(0.5); // Sets the engine sound to half volume.
        }
        else if (i == 10)
        {
            groupSliders[i].setValue(0.2); // Sets the reverb level to 20%.
        }
        else
        {
            groupSliders[i].setValue(1.0); // All other sliders are initialised with this value.
        }
    }
    
}

MainContentComponent::~MainContentComponent()
{
    objects.clearAndDelete();
}


void MainContentComponent::initFMODStudio()
{
    // Find the common resources path on this platform
    File resourcesPath = getResourcesPath();
    
    // Get the bank file paths
    File bankPath = resourcesPath.getChildFile ("Master Bank.bank");
    File stringsPath = resourcesPath.getChildFile ("Master Bank.strings.bank");
    
    // Open the FMOD System
    ERRCHECK (Studio::System::create (&system));
    ERRCHECK (system->getLowLevelSystem (&lowlevel));
    ERRCHECK (lowlevel->setSoftwareFormat (0, FMOD_SPEAKERMODE_5POINT1, 0));
    
    ERRCHECK (system->initialize (64,
                                  FMOD_STUDIO_INIT_LIVEUPDATE | FMOD_STUDIO_INIT_ALLOW_MISSING_PLUGINS,
                                  FMOD_INIT_PROFILE_ENABLE, 0)); // extraDriverData = 0
    
    // Load the bank into the system
    ERRCHECK (system->loadBankFile (bankPath.getFullPathName().toUTF8(),
                                    FMOD_STUDIO_LOAD_BANK_NORMAL,
                                    &bank));
    
    // Load the strings bank into the system - to translate from GUIDs
    ERRCHECK (system->loadBankFile (stringsPath.getFullPathName().toUTF8(),
                                    FMOD_STUDIO_LOAD_BANK_NORMAL,
                                    &stringsBank));
}

void MainContentComponent::shutdownFMODStudio()
{
    if (bank != nullptr)
    {
        ERRCHECK (bank->unload());
        bank = nullptr;
    }
    
    if (stringsBank != nullptr)
    {
        ERRCHECK (stringsBank->unload());
        stringsBank = nullptr;
    }
    
    if (system != nullptr)
        if (system->isValid())
            ERRCHECK (system->release());
    
    system = nullptr;
    lowlevel = nullptr;
}

void MainContentComponent::resized()
{
    int devider = 2;
    for (int i = 0; i < NUM_SLIDERS; i++)
    {
        if (i == 2 || i == 7) devider += 2; // Creates the gaps between the slider sections.
        groupSliders[i].setBounds(60, 25 * (i + devider), getWidth() - 70, 20);
    }
    
    masterGroupsLabel.setBounds(60, 25, getWidth() - 60, 25);
    carSoundsLabel.setBounds(60, 25 * 5, getWidth() - 60, 25);
    environmentSoundsLabel.setBounds(60, 25 * 12, getWidth() - 60, 25);
}

void MainContentComponent::handleConnect()
{
    initFMODStudio();
}

void MainContentComponent::handleDisconnect()
{
    shutdownFMODStudio();
    JUCEApplication::getInstance()->systemRequestedQuit();
}

void MainContentComponent::tick()
{
    // Here is our crash safe variable again.
    // Since the tick function is being called periodically,
    // we can use this area to perform "independently timed" operstions.
    if (collisionWait > 0)
        --collisionWait;
    
    // Check if we have a system, and if so, update it.
    if (system != nullptr)
        if (system->isValid())
            ERRCHECK (system->update());
}

void MainContentComponent::handleCreate (String const& name, int gameObjectInstanceID)
{
    // add the object to our dictionary of objects
    VectorData* vecData = new VectorData();
    objects.add (getGameInstanceString (name, gameObjectInstanceID), vecData);
    
    // get our info about the game object
    vecData = objects.get (getGameInstanceString (name, gameObjectInstanceID));
    
    // shouldn't happen as we should have added the object in handleCreate()
    // if we got as far as having a description
    jassert (vecData != nullptr);
    
    // Here the fun stuff begins..
    //
    //================ CREATE, ==============================================
    //============================= START, ==================================
    //========================================== ADD ========================
    //==================================================== ALL SOUNDS!!! ====
    EventDescription* tempDesc          = nullptr;
    
    if (name == "car")
    {
        // Make the engine howl!
        ERRCHECK_EXCEPT (system->getEvent ("event:/car/engine", &tempDesc), errExcept);
        if (tempDesc != nullptr)
        {
            // Create the event instance
            ERRCHECK (tempDesc->createInstance (&carEngineEventInstance));
            // Start the sound
            ERRCHECK (carEngineEventInstance->start());
            
            // Adjust the initial engine volume, because it hurts the ears otherwise.
            ERRCHECK(system->getBus("bus:/engine", &tempBus));
            ERRCHECK(tempBus->setFaderLevel(0.5));
            
            // And finally add it to our vector dictionary.
            vecData->addEvent(carEngineEventInstance);
        }
        
        // Make the tyres go iiiiiieek!
        ERRCHECK_EXCEPT (system->getEvent ("event:/car/skid", &tempDesc), errExcept);
        if (tempDesc != nullptr)
        {
            ERRCHECK (tempDesc->createInstance (&carSkidEventInstance));
            ERRCHECK (carSkidEventInstance->start());
            vecData->addEvent (carSkidEventInstance);
        }

        // We also need some wheels-on-road noise... yes, for each tyre individually... that's what the for loop is for.
        ERRCHECK_EXCEPT (system->getEvent ("event:/car/tyres", &tempDesc), errExcept);
        if (tempDesc != nullptr)
        {
            for (int i = 0; i < NUM_TYRES; i++)
            {
                ERRCHECK (tempDesc->createInstance (&carTyresEventInstance[i]));
                ERRCHECK (carTyresEventInstance[i]->start());
                vecData->addEvent (carTyresEventInstance[i]);
            }
        }
        
        // Some background wind noise. For a fully and utterly immersive gaming experience.
        ERRCHECK_EXCEPT (system->getEvent ("event:/environment/atmos", &tempDesc), errExcept);
        if (tempDesc != nullptr)
        {
            ERRCHECK (tempDesc->createInstance  (&atmosEventInstance));
            ERRCHECK (atmosEventInstance->start());
            vecData->addEvent(atmosEventInstance);
        }
    }
    
    // Make the people go "WOOOOOOOOAAAAAHHHH"
    if (name == "missioncontrol")
    {
        ERRCHECK_EXCEPT (system->getEvent ("event:/environment/crowd", &tempDesc), errExcept);
        if (tempDesc != nullptr)
        {
            ERRCHECK (tempDesc->createInstance  (&crowdEventInstance));
            ERRCHECK (crowdEventInstance->start());
            vecData->addEvent(crowdEventInstance);
        }
    }
    
    // Also, there are some electric wires spread over the track.
    // Let's make them "brzzzzzzzttt" when the car drives below.
    if (name == "wires")
    {
        ERRCHECK_EXCEPT (system->getEvent ("event:/environment/electric-wires", &tempDesc), errExcept);
        if (tempDesc != nullptr)
        {
            for (int i = 0; i < NUM_WIRES; i++)
            {
                if (gameObjectInstanceID == wiresIDs[i])
                {
                    ERRCHECK (tempDesc->createInstance(&wiresEventInstance[i]));
                    ERRCHECK (wiresEventInstance[i]->start());
                    vecData->addEvent(wiresEventInstance[i]);
                }
            }
        }
    }
    
    // There are a couple of locations that instinctively make us anticipate a change sound, influenced by the environment.
    // This is where some rad reverb kind of things come in very handy.
    if (name == "tunnel" || name == "underbridge" || name == "overbridge" || name == "missioncontrol")
    {
        // And because we have set up such nice string arrays,
        // we can throw all these reverb related setup function through a carefully crafted for loop.
        // Caution! Fragile. Handle with care.
        for (int i = 0; i < NUM_REVERBS; i++)
        {
            if (name == reverbLocationNames[i])
            {
                ERRCHECK_EXCEPT (system->getEvent (reverbEventPaths[i], &tempDesc), errExcept);
                if (tempDesc != nullptr)
                {
                    ERRCHECK (tempDesc->createInstance (&reverbInstances[i]));
                    ERRCHECK (reverbInstances[i]->start());
                    vecData->addEvent(reverbInstances[i]);
                }
            }
        }
        // Update the created reverbs variable, because after the fourth, we need to set the bus level
        createdReverbs += 1;
        if (createdReverbs == 4)
        {
            ERRCHECK(system->getBus("bus:/Reverb", &tempBus));
            ERRCHECK(tempBus->setFaderLevel(0.2));
        }
    }
}

void MainContentComponent::handleDestroy (String const& name, int gameObjectInstanceID)
{
    // remove the object from our dictionary of objects
    if (VectorData* vecData = objects.remove (getGameInstanceString (name, gameObjectInstanceID)))
        delete vecData;
}

void MainContentComponent::handleVector (String const& name, int gameObjectInstanceID, String const& param, const Vector3* vector)
{
    // So here we update all the vectordata for the objects that want their vectors updated.
    if (name == "camera")
    {
        // get current listnener attributes
        FMOD_3D_ATTRIBUTES attr3d;
        ERRCHECK(system->getListenerAttributes(FMOD_MAIN_LISTENER, &attr3d));
        
        // update the vector that we need
        if (param == "pos") attr3d.position = *vector;
        if (param == "vel") attr3d.velocity = *vector;
        if (param == "dir") attr3d.forward  = *vector;
        if (param == "up")  attr3d.up       = *vector;
        
        // set the updated attributes
        ERRCHECK (system->setListenerAttributes(FMOD_MAIN_LISTENER, &attr3d));
    }
    else
    {
        // get our info about the game object
        VectorData* vecData = objects.get (getGameInstanceString(name, gameObjectInstanceID));
        
        if (vecData != nullptr)
        {
            if (param == "pos") vecData->setVectors(vector, nullptr, nullptr);
            if (param == "vel") vecData->setVectors(nullptr, vector, nullptr);
            if (param == "dir") vecData->setVectors(nullptr, nullptr, vector);
            if (param == "up")  vecData->setVectors(nullptr, nullptr, nullptr, vector);
        }
    }
}

void MainContentComponent::handleHit (String const& name, int gameObjectInstanceID, Collision const& collision)
{
    // If the car hits anything, we want to hear some interesting sound.
    // All this happens in this function.
    
    // Because we want to avoid hearing crash sounds upon startup,
    // let's make this function wait for a little while.
    if (collisionWait > 0)
        return;
    
    EventDescription* desc = nullptr;
    
    // Try to get an event description for the object.
    if (name == "car")
    {
        // General crash with Untagged.
        if (collision.otherName == "Untagged")
        {
            ERRCHECK_EXCEPT (system->getEvent ("event:/car/crash", &desc), errExcept);
        }
    }
   
    
    // If we got a description then we deal with it.
    if (desc != nullptr)
    {
        // Get our info about the game object.
        VectorData* vecData = objects.get (getGameInstanceString (name, gameObjectInstanceID));
        
        // This shouldn't ever happen as we should have added the object in handleCreate()
        // if we got as far as having a description.
        jassert (vecData != nullptr);
        
        // Create an instance.
        EventInstance* instance = nullptr;
        ERRCHECK (desc->createInstance (&instance));
        
        // Update the intensity parameter.
        ERRCHECK_EXCEPT (instance->setParameterValue ("intensity", collision.velocity), errExcept);
        
        // Start the event.
        ERRCHECK (instance->start());
        
        // Add it to our game object store, we must start it before adding it otherwise
        // our VectorData class might remove it before it gets a chance to play as it
        // will be initially marked as "stopped".
        vecData->addEvent (instance);
    }
}

//void MainContentComponent::handleBool (String const& name, int gameObjectInstanceID, String const& param, bool flag)
//{
//  //This is a placeholder function for future development.
//}
//

void MainContentComponent::handleInt (String const& name, int gameObjectInstanceID, String const& param, int value)
{
    // Similar to handleHit(), but for changes of integer values such as gear changes.
    
    EventDescription* desc = nullptr;
    
    // Try to get an event description for the object.
    if (name == "car")
    {
        // General crash with Untagged.
        if (param == "gear")
        {
            ERRCHECK_EXCEPT (system->getEvent ("event:/car/gear", &desc), errExcept);
        }
    }
    
    
    // If we got a description then we deal with it.
    if (desc != nullptr)
    {
        // Get our info about the game object.
        VectorData* vecData = objects.get (getGameInstanceString (name, gameObjectInstanceID));
        
        // This shouldn't ever happen as we should have added the object in handleCreate()
        // if we got as far as having a description.
        jassert (vecData != nullptr);
        
        // Create an instance.
        EventInstance* instance = nullptr;
        ERRCHECK (desc->createInstance (&instance));
        
        // Update the intensity parameter.
        ERRCHECK_EXCEPT (instance->setParameterValue ("blowoff", value), errExcept);
        
        // Start the event.
        ERRCHECK (instance->start());
        
        // Sdd it to our game object store, we must start it before adding it otherwise
        // our VectorData class might remove it before it gets a chance to play as it
        // will be initially marked as "stopped".
        vecData->addEvent (instance);
    }
}

void MainContentComponent::handleReal (String const& name, int gameObjectInstanceID, String const& param, double value)
{
    // Rapidly changing values such as rpm, speed, etc. are handled here.
    
    if (name == "car")
    {
        if (param == "rpm")
        {   // if this is the parameter that changed in the game engine, update our fmod sound accordingly.
            ERRCHECK_EXCEPT (carEngineEventInstance->setParameterValue ("rpm", value), errExcept);
        }
        else if (param == "torque")
        {
            ERRCHECK_EXCEPT (carEngineEventInstance->setParameterValue ("load", value), errExcept);
        }
        else if (param == "skid")
        {
            ERRCHECK_EXCEPT (carSkidEventInstance->setParameterValue ("amount", value), errExcept);
        }
        else if (param == "speed")
        {
            for (int i = 0; i < NUM_TYRES; i++)
            {
                ERRCHECK_EXCEPT (carTyresEventInstance[i]->setParameterValue ("speed", value), errExcept);
            }
            
            // Adjust the crowd noise dependent on the car's speed.
            // Because if the car goes WHOOOSHH, the crowd goes WHOOOOOO!!!
            // But if the car goes brumm brumm, the crowd goes ... meh...
            double currentSpeedPercent = value/100;
            ERRCHECK(system->getBus("bus:/crowd", &tempBus));
            ERRCHECK(tempBus->setFaderLevel(currentSpeedPercent));
        }
    }
    else if (compareToTyreName(name))
    {
        for (int i = 0; i < NUM_TYRES; i++)
        {
            if (name == tyreNames[i] && param == "force")
            {
                ERRCHECK_EXCEPT (carTyresEventInstance[i]->setParameterValue ("force", value), errExcept);
            }
        }
    }
}

//void MainContentComponent::handleString (String const& name, int gameObjectInstanceID, String const& param, String const& content)
//{
//  //This is a placeholder function for future development.

//}
//
//void MainContentComponent::handleOther (String const& name, String const& t, String const& value)
//{
//  //This is a placeholder function for future development.

//}

void MainContentComponent::sliderValueChanged(juce::Slider *slider)
{
    // We have a bunch of sliders in our JUCE interface, resambling, what some might say, a mixer.
    // If we fiddle around with any of these, we handle the value change in here accordingly.
    if (collisionWait > 0)
        return;
    
    // Each slider is assigned to a VCA or Bus group.
    // When the specific slider moves, the group level gets adjusted here.
    for (int i = 0; i < NUM_SLIDERS; i++)
    {
        if (slider == &groupSliders[i])
        {
            if (i < 2)
            {
                ERRCHECK(system->getVCA(channelGroupPaths[i], &tempVCA));
                ERRCHECK(tempVCA->setFaderLevel(slider->getValue()));
            }
            else
            {
                ERRCHECK(system->getBus(channelGroupPaths[i], &tempBus));
                ERRCHECK(tempBus->setFaderLevel(slider->getValue()));
            }
        }
    }
}

// This function has the sole purpose to not having to write a very long condition in some if statement above.
// It really only compares a given string (name) to strings in an array (tyreNames) and returns true if any of these match.
// And false otherwise.
bool MainContentComponent::compareToTyreName(String const& name)
{
    for (int i = 0; i < NUM_TYRES; i++)
    {
        if (name == tyreNames[i]) return true;
    }
    return false;
}




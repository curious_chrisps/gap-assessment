//
//  VectorData.hpp
//  racing
//
//  Created by Christoph Schick on 27/02/2018.
//

#ifndef VectorData_hpp
#define VectorData_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../../../common/FMODHeaders.h"

using namespace FMOD::Studio;

/** Assert on results that are not OK and not in the errorExceptions array. */
static bool ERRCHECK_EXCEPT (FMOD_RESULT result, Array<FMOD_RESULT> const& errorExceptions)
{
    if (result == FMOD_OK)                  return true;
    if (errorExceptions.contains (result))  return false;
    
    ERRCHECK (result);
    return false;
}

/** Keep track of the vector data for a game object.
 This also keeps an array of EventInstance objects that are already playing at
 this object's position and automatically updates the 3D attributes
 when the object moves. It also removes the EventInstance from the array
 when the EventInstance has finished playing. Functions are provided to start,
 stop, and apply trigger cues for all current events.
 You can add more functions if you need to modify all events in other ways.
 */
class VectorData
{
public:
    
    /** Create a zero'd object.
     The up vector is set to point in the y direction. */
    VectorData()
    {
        memset (&attr3d, 0, sizeof (attr3d));
        attr3d.up.y = 1.0f;
        attr3d.forward.z = 1.0f;
        
        // ignore errors if objects need 3D an aren't
        err3DExcept.add (FMOD_ERR_NEEDS3D);
        
        // we'll ignore errors if parameters are missing
        errParamExcept.add (FMOD_ERR_EVENT_NOTFOUND);
    }
    
    ~VectorData()
    {
        stopEvents();
    }
    
    /** Check if an event is still playing.
     If not, it is removed from our array. */
    bool eventIsLive (EventInstance* event)
    {
        FMOD_STUDIO_PLAYBACK_STATE state;
        FMOD_RESULT error = event->getPlaybackState (&state);
        
        if ((error == FMOD_OK) &&
            (state != FMOD_STUDIO_PLAYBACK_STOPPED))
        {
            return true;
        }
        else
        {
            events.removeFirstMatchingValue (event);
            event->release();
            return false;
        }
    }
    
    /** Update one or more of the vectors.
     Pass nullptr in any of these arguments to be ignored. */
    void setVectors (const Vector3 *newPos,
                     const Vector3 *newVel,
                     const Vector3 *newFwd,
                     const Vector3 *newUp = nullptr)
    {
        if (newPos != nullptr) attr3d.position = *newPos;
        if (newVel != nullptr) attr3d.velocity = *newVel;
        if (newFwd != nullptr) attr3d.forward  = *newFwd;
        if (newUp  != nullptr) attr3d.up       = *newUp;
        
        for (int i = events.size(); --i >= 0;)
        {
            EventInstance* event = events.getUnchecked (i);
            
            if (eventIsLive (event))
                ERRCHECK_EXCEPT (event->set3DAttributes (&attr3d), err3DExcept);
        }
    }
    
    /** Get the current vectors. */
    FMOD_3D_ATTRIBUTES getVector()
    {
        return attr3d;
    }
    
    /** Add an EventInstance playing at this object position. */
    void addEvent (EventInstance* event)
    {
        // add to the array
        events.add (event);
        
        // set its vectors
        ERRCHECK_EXCEPT (event->set3DAttributes (&attr3d), err3DExcept);
    }
    
    /** Remove an EventInstance manually. */
    void removeEvent (EventInstance* event)
    {
        events.removeFirstMatchingValue (event);
    }
    
    /** Call start() on all of the EventInstance objects. */
    void startEvents()
    {
        for (int i = events.size(); --i >= 0;)
            ERRCHECK (events.getUnchecked (i)->start());
    }
    
    /** Call stop() on all of the EventInstance objects.
     This allows events to fade out. It also releases the event instances so
     they are deleted from memory when the actually stop. */
    void stopEvents()
    {
        for (int i = events.size(); --i >= 0;)
        {
            EventInstance* event = events.getUnchecked (i);
            
            if (eventIsLive (event))
            {
                ERRCHECK (event->stop (FMOD_STUDIO_STOP_ALLOWFADEOUT));
                ERRCHECK (event->release());
            }
        }
    }
    
    /** Trigger cue "0" on all of the EventInstance objects. */
    void triggerCues()
    {
        for (int i = events.size(); --i >= 0;)
        {
            EventInstance* event = events.getUnchecked (i);
            
            if (eventIsLive (event))
            {
                event->triggerCue();
            }
        }
    }
    
    /** Set a particular parameter to a value in all of the EventInstance objects. */
    void setParameter (String const& param, const float value)
    {
        const char* paramString = param.toRawUTF8();
        
        for (int i = events.size(); --i >= 0;)
        {
            EventInstance* event = events.getUnchecked (i);
            
            if (eventIsLive (event))
                ERRCHECK_EXCEPT (event->setParameterValue (paramString, value), errParamExcept);
        }
    }
    
    
private:
    FMOD_3D_ATTRIBUTES attr3d;    ///< The object vectors for position, velocity, forward (orientation), and "up" (also orientation)
    Array<EventInstance*> events; ///< The array of event instances
    
    Array<FMOD_RESULT> err3DExcept;
    Array<FMOD_RESULT> errParamExcept;
};

#endif /* VectorData_hpp */

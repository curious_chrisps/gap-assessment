#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../../../common/FMODHeaders.h"
#include "VectorData.hpp"
#define NUM_TYRES 4
#define NUM_WIRES 4
#define NUM_SLIDERS 11
#define NUM_REVERBS 4
using namespace FMOD::Studio;

/** Simple typdef to avoid typing cumbersome type names. */
typedef PointerDictionary<VectorData> VectorDictionary;

class MainContentComponent  : public Component,
                              public GameEngineServer,
                              public Slider::Listener
{
//==================== PRIVATE =============================
private:
    //========================================== JUCE objects =============================
    Label masterGroupsLabel;
    Label carSoundsLabel;
    Label environmentSoundsLabel;

    // Arrays of sliders and labels.
    Slider groupSliders[NUM_SLIDERS];
    Label labelsArray[NUM_SLIDERS];
    
    
    //========================================== FMOD objects =============================
    Studio::System* system;
    Bank* bank;
    Bank* stringsBank;
    FMOD::System *lowlevel;
    
    // VCA and Bus objects to control the levels of different groups.
    Studio::VCA* tempVCA;
    Studio::Bus* tempBus;
    
    //========================================= Event instances ===========================
    // Car EventInstances
    Studio::EventInstance* carEngineEventInstance;
    Studio::EventInstance* carSkidEventInstance;
    Studio::EventInstance* carTyresEventInstance[NUM_TYRES]; // one EventInstance for each tyre on the car
    Studio::EventInstance* wiresEventInstance[NUM_WIRES];    // one EventInstance for each wire on the track
    
    // Ambience EventInstence
    Studio::EventInstance* atmosEventInstance;
    Studio::EventInstance* crowdEventInstance;
    
    // reverb event instences
    Studio::EventInstance* reverbInstances[NUM_REVERBS];
    
    
    // A "dictionary" to store game objects we need to refer to
    VectorDictionary objects;
    
    // An array of FMOD_RESULT errors to ignore in some cases
    Array<FMOD_RESULT> errExcept;
    
    // Countdown variable. Used in various places to make sure some functions "wait"
    // for the game to be ready before start executing sensitive functions.
    int collisionWait;
    int createdReverbs = 0;
    
    
    /*
     =================================================
     Numerous string arrays to enable neater,
     more efficient code in the .cpp file.
     =================================================
     */
    String labelStrings[NUM_SLIDERS] = {
        "Car VCA",      //[0]
        "Atmo VCA",     //[1]
        "Engine",       //[2]
        "Skid",         //[3]
        "Tyres",        //[4]
        "Gear",         //[5]
        "Crash",        //[6]
        "Atmos",        //[7]
        "Crowd",        //[8]
        "Wires",        //[9]
        "Reverb"        //[10]
    };
    
    String reverbLocationNames[NUM_REVERBS] = {
        "tunnel",         //[0]
        "underbridge",    //[1]
        "overbridge",     //[2]
        "missioncontrol"  //[3]
    };
    
    String tyreNames[4] = {
        "WheelFR",  //[0]
        "WheelFL",  //[1]
        "WheelRR",  //[2]
        "WheelRL"   //[3]
    };
    
    
    int wiresIDs[NUM_WIRES] = {
        11126,  //[0]
        11128,  //[1]
        11130,  //[2]
        11132   //[3]
    };
    
    const char* channelGroupPaths[NUM_SLIDERS] = {
        "vca:/Car",             //[0] is VCA!!!
        "vca:/Environment",     //[1] is VCA!!!
        "bus:/engine",          //[2] from here on down all busses!
        "bus:/skid",            //[3]
        "bus:/tyres",           //[4]
        "bus:/gear",            //[5]
        "bus:/crash",           //[6]
        "bus:/atmos",           //[7]
        "bus:/crowd",           //[8]
        "bus:/electric-wires",  //[9]
        "bus:/Reverb"           //[10]
    };
    
    const char* reverbEventPaths[NUM_REVERBS] = {
        "event:/reverbs/tunnel",          //[0]
        "event:/reverbs/underbridge",     //[1] 
        "event:/reverbs/overbridge",      //[2]
        "event:/reverbs/missioncontrol"   //[3]
    };
   
    
//==================== PUBLIC =============================
public:
    MainContentComponent();
    ~MainContentComponent();
	
    void initFMODStudio();
    void shutdownFMODStudio();
	
    void resized() override;
    
    void handleConnect() override;
    void handleDisconnect() override;
    void tick() override;
    
    void handleCreate (String const& name, int gameObjectInstanceID) override;
    void handleDestroy (String const& name, int gameObjectInstanceID) override;
    void handleVector (String const& name, int gameObjectInstanceID, String const& param, const Vector3* vector) override;
    void handleHit (String const& name, int gameObjectInstanceID, Collision const& collision) override;
//    void handleBool (String const& name, int gameObjectInstanceID, String const& param, bool flag) override;
    void handleInt (String const& name, int gameObjectInstanceID, String const& param, int value) override;
    void handleReal (String const& name, int gameObjectInstanceID, String const& param, double value) override;
//    void handleString (String const& name, int gameObjectInstanceID, String const& param, String const& content) override;
//    void handleOther (String const& name, String const& t, String const& value) override;
    
    
    void sliderValueChanged (Slider* slider) override;
    
    // checker function
    bool compareToTyreName(String const& name);
};

#endif  // MAINCOMPONENT_H_INCLUDED
